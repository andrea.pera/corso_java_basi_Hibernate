package corso_java_basi_Hibernate;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.controller.AppController;
import com.model.entity.Indirizzo;
import com.model.entity.Proprietario;
import com.model.entity.Utente;

public class TestIndirizzo {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TestIndirizzo.class);
	private AppController controller;
	
	@Before
	public void setUp() {
		controller = AppController.getInstance();
	}
	
	@Test
	public void testAddIndirizzo() {
		
		Utente proprietario = controller.addProprietario("Nome_Nuovo_Proprietario", "Cognome_Nuovo_Proprietario");
		Utente meccanico = controller.addMeccanico("Nome_Meccanico", "Cognome_Meccanico", "Specializzazione_Meccanico");
		
		// Eseguo la Funzionalita da testare con il Proprietario:
		Indirizzo indirizzo_1 = controller.addIndirizzo("Mia_Nazione_1", "Mia_Citta_1", "Mia_Via_1", 1, proprietario);
		assertTrue(indirizzo_1.getId() > 0); // Se l'Asserzione non è vera lancia l'eccezione ed il codice di seguito non viene eseguito.
		
		LOGGER.info("Indirizzo " + indirizzo_1.getId() + " Aggiunto al DB!");
		System.out.println(indirizzo_1.toString());

		// Eseguo la Funzionalita da testare con il Meccanico:
		Indirizzo indirizzo_2 = controller.addIndirizzo("Mia_Nazione_2", "Mia_Citta_2", "Mia_Via_2", 2, meccanico);
		assertTrue(indirizzo_2.getId() > 0);
		
		LOGGER.info("Indirizzo " + indirizzo_2.getId() + " Aggiunto al DB!");
		System.out.println(indirizzo_2.toString());
	}
	
	@Test
	public void testRemoveIndirizzo() {
		
		Utente utente = controller.addProprietario("Nome_Nuovo_Proprietario", "Cognome_Nuovo_Proprietario");
		Indirizzo indirizzo = controller.addIndirizzo("Mia_Nuova_Nazione", "Mia_Nuova_Citta", "Mia_Nuova_Via", 1, utente);
		
		// Eseguo la Funzionalita da testare:
		boolean result = controller.removeIndirizzo(indirizzo.getId());
		assertTrue(result);
		//proprietario = controller.findProprietario(proprietario.getId());
		//assertNull(proprietario);
		
		LOGGER.info("Indirizzo " + indirizzo.getId() + " Rimosso dal DB!");
	}
	
	@Test
	public void testUpdateIndirizzo() {
		
		Utente utente = controller.addProprietario("Nome_Nuovo_Proprietario", "Cognome_Nuovo_Proprietario");
	    Indirizzo indirizzo = controller.addIndirizzo("Mia_Nazione", "Mia_Citta", "Mia_Via", 1, utente);
		
	    // Eseguo la Funzionalita da testare:
		boolean result = controller.updateIndirizzo(indirizzo.getId(), "Mia_Nuova_Nazione", "Mia_Nuova_Citta", "Mia_Nuova_Via", 1, utente);
		assertTrue(result);
		
		LOGGER.info("Indirizzo " + indirizzo.getId() + " Aggiornato nel DB!");
	}
	
	@Test
	public void testFindIndirizzo() {
		
		Utente utente = controller.addProprietario("Nome_Nuovo_Proprietario", "Cognome_Nuovo_Proprietario");
		Indirizzo indirizzo = controller.addIndirizzo("Mia_Nazione", "Mia_Citta", "Mia_Via", 1, utente);
		int id_indirizzo = indirizzo.getId();
		
		// Eseguo la Funzionalita da testare:
		indirizzo = controller.findIndirizzo(id_indirizzo);
		assertNotNull(indirizzo);
		
		LOGGER.info("Indirizzo " + id_indirizzo + " Trovato nel DB!");
		System.out.println(indirizzo.toString());
	}
	
	@Test
	public void testFindAllIndirizzi() {
		
		Utente utente_1 = controller.addProprietario("Nome_Proprietario_1", "Cognome_Proprietario_1");
		controller.addIndirizzo("Nazione_1", "Citta_1", "Via_1", 1, utente_1);
		Utente utente_2 = controller.addProprietario("Nome_Proprietario_2", "Cognome_Proprietario_2");
		controller.addIndirizzo("Nazione_2", "Citta_2", "Via_2", 2, utente_2);
		Utente utente_3 = controller.addProprietario("Nome_Proprietario_3", "Cognome_Proprietario_3");
		controller.addIndirizzo("Nazione_3", "Citta_3", "Via_3", 3, utente_3);
		
		// Eseguo la Funzionalita da testare:
		List<Indirizzo> lista_indirizzi = controller.getAllIndirizzi();
		assertNotNull(lista_indirizzi);
		
		LOGGER.info("Indirizzi Trovati nel DB!");
		for(Indirizzo indirizzo: lista_indirizzi) {
			System.out.println(indirizzo.toString());
		}
	}

}
