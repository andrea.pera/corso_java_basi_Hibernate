package corso_java_basi_Hibernate;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.controller.AppController;
import com.model.entity.Automobile;
import com.model.entity.Indirizzo;
import com.model.entity.Proprietario;

public class TestAutomobile {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TestAutomobile.class);
	private AppController controller;
	
	@Before
	public void setUp() {
		controller = AppController.getInstance();
	}
	
	@Test
	public void testAddAutomobile() {
		
		Proprietario proprietario = controller.addProprietario("Nome_Nuovo_Proprietario", "Cognome_Nuovo_Proprietario");
		Indirizzo indirizzo = controller.addIndirizzo("Mia_Nazione", "Mia_Citta", "Mia_Via", 1, proprietario);
		proprietario.setIndirizzo(indirizzo);
		
		// Eseguo la Funzionalita da testare:
		Automobile automobile = controller.addAuto("Nuovo_Modello", "Nuova_Marca", 150, true, proprietario);
		assertTrue(automobile.getId() > 0); // Se l'Asserzione non è vera lancia l'eccezione ed il codice di seguito non viene eseguito.
		
		LOGGER.info("Automobile " + automobile.getId() + " Aggiunta al DB!");
		System.out.println(automobile.toString());
	}
	
	@Test
	public void testRemoveAutomobile() {
		
		Proprietario proprietario = controller.addProprietario("Nome_Nuovo_Proprietario", "Cognome_Nuovo_Proprietario");
		Indirizzo indirizzo = controller.addIndirizzo("Mia_Nuova_Nazione", "Mia_Nuova_Citta", "Mia_Nuova_Via", 1, proprietario);
		proprietario.setIndirizzo(indirizzo);
		Automobile automobile = controller.addAuto("Nuovo_Modello", "Nuova_Marca", 150, true, proprietario);
		
		// Eseguo la Funzionalita da testare:
		boolean result = controller.removeAuto(automobile.getId());
		assertTrue(result);
		proprietario = controller.findProprietario(proprietario.getId());
		assertNotNull(proprietario); // Il Proprietario dell'Automobile cancellata non dovrebbe essere stato cancellato.
		indirizzo = controller.findIndirizzo(indirizzo.getId());
		assertNotNull(indirizzo);
		
		LOGGER.info("Automobile " + automobile.getId() + " Rimossa dal DB!");
	}
	
	@Test
	public void testUpdateAutomobile() {
		
		Proprietario proprietario_1 = controller.addProprietario("Nome_Proprietario_1", "Nuovo_Proprietario_1");
	    Indirizzo indirizzo_1 = controller.addIndirizzo("Nazione_1", "Citta_1", "Via_1", 1, proprietario_1);
		proprietario_1.setIndirizzo(indirizzo_1);
		Automobile automobile = controller.addAuto("Nuovo_Modello", "Nuova_Marca", 150, true, proprietario_1);
		
		Proprietario proprietario_2 = controller.addProprietario("Nome_Proprietario_2", "Nuovo_Proprietario_2");
	    Indirizzo indirizzo_2 = controller.addIndirizzo("Nazione_2", "Citta_2", "Via_2", 2, proprietario_2);
	    proprietario_2.setIndirizzo(indirizzo_2);
		
	    // Eseguo la Funzionalita da testare:
		boolean result = controller.updateAuto(automobile.getId(), "Mia_Nuovo_Modello", "Mia_Nuova_Marca", 70, false, proprietario_2);
		assertTrue(result);
		
		LOGGER.info("Automobile " + automobile.getId() + " Aggiornata nel DB!");
	}
	
	@Test
	public void testFindAutomobile() {
		
		Proprietario proprietario = controller.addProprietario("Nome_Nuovo_Proprietario", "Cognome_Nuovo_Proprietario");
		Indirizzo indirizzo = controller.addIndirizzo("Mia_Nazione", "Mia_Citta", "Mia_Via", 1, proprietario);
		proprietario.setIndirizzo(indirizzo);
		Automobile automobile = controller.addAuto("Nuovo_Modello", "Nuova_Marca", 150, true, proprietario);
		int id_automobile = automobile.getId();
		
		// Eseguo la Funzionalita da testare:
		automobile = controller.findAuto(id_automobile);
		assertNotNull(automobile);
		
		LOGGER.info("Automobile " + id_automobile + " Trovata nel DB!");
		System.out.println(automobile.toString());
	}
	
	@Test
	public void testFindAllAutomobili() {
		
		Proprietario proprietario_1 = controller.addProprietario("Nome_Proprietario_1", "Cognome_Proprietario_1");
		Indirizzo indirizzo_1 = controller.addIndirizzo("Nazione_1", "Citta_1", "Via_1", 1, proprietario_1);
		proprietario_1.setIndirizzo(indirizzo_1);
		controller.addAuto("Modello_1", "Marca_1", 51, false, proprietario_1);
		
		Proprietario proprietario_2 = controller.addProprietario("Nome_Proprietario_2", "Cognome_Proprietario_2");
		Indirizzo indirizzo_2 = controller.addIndirizzo("Nazione_2", "Citta_2", "Via_2", 2, proprietario_2);
		proprietario_2.setIndirizzo(indirizzo_2);
		controller.addAuto("Modello_2", "Marca_2", 52, false, proprietario_2);
		
		Proprietario proprietario_3 = controller.addProprietario("Nome_Proprietario_3", "Cognome_Proprietario_3");
		Indirizzo indirizzo_3 = controller.addIndirizzo("Nazione_3", "Citta_3", "Via_3", 3, proprietario_3);
		proprietario_3.setIndirizzo(indirizzo_3);
		controller.addAuto("Modello_3", "Marca_3", 53, false, proprietario_3);
		
		// Eseguo la Funzionalita da testare:
		List<Automobile> lista_automobili = controller.getAllAutomobili();

		assertNotNull(lista_automobili);
		
		LOGGER.info("Indirizzi Trovati nel DB!");
		for(Automobile automobile: lista_automobili) {
			System.out.println(automobile.toString());
		}
	}

}
