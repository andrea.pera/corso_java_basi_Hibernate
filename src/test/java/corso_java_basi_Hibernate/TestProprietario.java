package corso_java_basi_Hibernate;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Before;
import org.junit.Test;
import com.controller.AppController;
import com.model.entity.Automobile;
import com.model.entity.Indirizzo;
import com.model.entity.Meccanico;
import com.model.entity.Proprietario;
import com.model.util.Proprietario_Meccanico;

/**
 * Test per le operazione sul DB relative al Proprietario.
 */
public class TestProprietario {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TestProprietario.class);
	private AppController controller;
	
	@Before
	public void setUp() {
		controller = AppController.getInstance();
	}
	
	@Test
	public void testAddProprietario() {
		
		// Eseguo la Funzionalita da testare:
		Proprietario proprietario = controller.addProprietario("Nome_Nuovo_Proprietario", "Cognome_Nuovo_Proprietario");
		controller.addIndirizzo("Mia_Nazione", "Mia_Citta", "Mia_Via", 1, proprietario);
		assertTrue(proprietario.getId() > 0); // Se l'Asserzione non è vera lancia l'eccezione ed il codice di seguito non viene eseguito.
		
		LOGGER.info("Proprietario " + proprietario.getId() + " Aggiunto al DB!");
		System.out.println(proprietario.toString());
	}
	
	@Test
	public void testRemoveProprietario() {
		
		Proprietario proprietario = controller.addProprietario("Nome_Proprietario", "Cognome_Proprietario");
		Indirizzo indirizzo = controller.addIndirizzo("Mia_Nazione", "Mia_Citta", "Mia_Via", 1, proprietario);
		controller.addAuto("Nome_Modello", "Nome_Marca", 200, true, proprietario);
		Meccanico meccanico_1 = controller.addMeccanico("Nome_Meccanico_1", "Cognome_Meccanico_1", "Specializzazione_Meccanico_1");
		Meccanico meccanico_2 = controller.addMeccanico("Nome_Meccanico_2", "Cognome_Meccanico_2", "Specializzazione_Meccanico_2");

		// Inserisco l'associazione nella Tabella di Join/Appoggio tra il Meccanico ed il Proprietario:
		controller.addAssociationProprietarioMeccanico(new Proprietario_Meccanico(proprietario.getId(), meccanico_1.getId()));
		controller.addAssociationProprietarioMeccanico(new Proprietario_Meccanico(proprietario.getId(), meccanico_2.getId()));
		
		// Eseguo la Funzionalita da testare:
		boolean result = controller.removeProprietario(proprietario.getId());
		assertTrue(result);
		
		indirizzo = controller.findIndirizzo(indirizzo.getId());
		assertNull(indirizzo); // L'Indirizzo dovrebbe essere stato cancellato a CASCATA (fatto automaticamente da Hibernate).
		
		List<Automobile> lista_automobili = controller.findAutomobiliByProprietarioId(proprietario.getId());
		assertTrue(lista_automobili.isEmpty()); // Tutte le associazioni di quel Proprietario rimosso con le Automobili dovrebbe essere state cancellate.
		
		List<Proprietario_Meccanico> lista_associazioni = controller.findAssociationsByProprietarioId(proprietario.getId());
		assertTrue(lista_associazioni.isEmpty()); // Tutte le associazioni con quel Proprietario rimosso dovrebbe essere state cancellate dalla tabella di Join/Appoggio.
		
		LOGGER.info("Proprietario " + proprietario.getId() + " Rimosso dal DB!");
	}
	
	@Test
	public void testUpdateProprietario() {
		
		Proprietario proprietario = controller.addProprietario("Nome_Nuovo_Proprietario", "Cognome_Nuovo_Proprietario");
		controller.addIndirizzo("Mia_Nazione", "Mia_Citta", "Mia_Via", 1, proprietario);
		
		// Eseguo la Funzionalita da testare:
		boolean result = controller.updateProprietario(proprietario.getId(), "Nome_Proprietario_Aggiornato", "Cognome_Proprietario_Aggiornato");
		assertTrue(result);
		
		LOGGER.info("Proprietario " + proprietario.getId() + " Aggiornato nel DB!");
	}
	
	@Test
	public void testFindProprietario() {
		
		Proprietario proprietario = controller.addProprietario("Nome_Nuovo_Proprietario", "Cognome_Nuovo_Proprietario");
		controller.addIndirizzo("Mia_Nazione", "Mia_Citta", "Mia_Via", 1, proprietario);
		int id_proprietario = proprietario.getId();
		
		// Eseguo la Funzionalita da testare:
		proprietario = controller.findProprietario(id_proprietario);
		assertNotNull(proprietario);
		
		LOGGER.info("Proprietario " + id_proprietario + " Trovato nel DB!");
		System.out.println(proprietario.toString());
	}
	
	@Test
	public void testFindAllProprietari() {
		
		Proprietario proprietario_1 = controller.addProprietario("Nome_Proprietario_1", "Cognome_Proprietario_1");
		controller.addIndirizzo("Nazione_1", "Citta_1", "Via_1", 1, proprietario_1);
		Proprietario proprietario_2 = controller.addProprietario("Nome_Proprietario_2", "Cognome_Proprietario_2");
		controller.addIndirizzo("Nazione_2", "Citta_2", "Via_2", 2, proprietario_2);
		Proprietario proprietario_3 = controller.addProprietario("Nome_Proprietario_3", "Cognome_Proprietario_3");
		controller.addIndirizzo("Nazione_3", "Citta_3", "Via_3", 3, proprietario_3);
		
		// Eseguo la Funzionalita da testare:
		List<Proprietario> lista_proprietari = controller.getAllProprietari();
		assertNotNull(lista_proprietari);
		
		LOGGER.info("Proprietari Trovati nel DB!");
		for(Proprietario proprietario: lista_proprietari) {
			System.out.println(proprietario.toString());
		}
	}
	
	
}
