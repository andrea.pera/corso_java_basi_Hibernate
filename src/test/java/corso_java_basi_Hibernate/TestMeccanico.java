package corso_java_basi_Hibernate;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.controller.AppController;
import com.model.entity.Indirizzo;
import com.model.entity.Meccanico;
import com.model.entity.Proprietario;
import com.model.util.Proprietario_Meccanico;

/**
 * Test per le operazione sul DB relative al Meccanico.
 */
public class TestMeccanico {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TestMeccanico.class);
	private AppController controller;
	
	@Before
	public void setUp() {
		controller = AppController.getInstance();
	}
	
	@Test
	public void testAddMeccanico() {
		
		Proprietario proprietario = controller.addProprietario("Mario", "Rossi");
		Indirizzo indirizzo_1 = controller.addIndirizzo("Italia", "Roma", "Via Vittorio Emanuele", 21, proprietario);
		proprietario.setIndirizzo(indirizzo_1);
		
		// Eseguo la Funzionalita da testare:
		Meccanico meccanico = controller.addMeccanico("Peppe", "Bianchi", "Carozziere");
		assertTrue(meccanico.getId() > 0); // Se l'Asserzione non è vera lancia l'eccezione ed il codice di seguito non viene eseguito.
		
		Indirizzo indirizzo_2 = controller.addIndirizzo("Italia", "Roma", "Via Garibaldi", 1, meccanico);
		meccanico.setIndirizzo(indirizzo_2);
		meccanico.setProprietari(new HashSet<Proprietario>(Arrays.asList(proprietario)));
		
		LOGGER.info("Meccanico " + meccanico.getId() + " Aggiunto al DB!");
		System.out.println(meccanico.toString());
		
		// Inserisco l'associazione nella Tabella di Join/Appoggio tra il Meccanico ed il Proprietario:
		assertTrue(controller.addAssociationProprietarioMeccanico(new Proprietario_Meccanico(proprietario.getId(), meccanico.getId())));
		
		LOGGER.info("Associazione tra Meccanico " + meccanico.getId() + " e Proprietario " + proprietario.getId() + " Aggiunta al DB!");
	}
	
	@Test
	public void testRemoveMeccanico() {
		
		Proprietario proprietario_1 = controller.addProprietario("Nome_Proprietario_1", "Cognome_Proprietario_1");
		Proprietario proprietario_2 = controller.addProprietario("Nome_Proprietario_2", "Cognome_Proprietario_2");
		Indirizzo indirizzo_1 = controller.addIndirizzo("Mia_Nazione_1", "Mia_Citta_1", "Mia_Via_1", 1, proprietario_1);
		Indirizzo indirizzo_2 = controller.addIndirizzo("Mia_Nazione_2", "Mia_Citta_2", "Mia_Via_2", 2, proprietario_2);
		proprietario_1.setIndirizzo(indirizzo_1);
		proprietario_2.setIndirizzo(indirizzo_2);
		Meccanico meccanico = controller.addMeccanico("Nome_Meccanico", "Cognome_Meccanico", "Specializzazione_Meccanico");
		Indirizzo indirizzo_3 = controller.addIndirizzo("Mia_Nazione_3", "Mia_Citta_3", "Mia_Via_3", 3, meccanico);
		meccanico.setIndirizzo(indirizzo_3);
		
		// Inserisco l'associazione nella Tabella di Join/Appoggio tra il Meccanico ed il Proprietario:
		controller.addAssociationProprietarioMeccanico(new Proprietario_Meccanico(proprietario_1.getId(), meccanico.getId()));
		controller.addAssociationProprietarioMeccanico(new Proprietario_Meccanico(proprietario_2.getId(), meccanico.getId()));
		
		// Eseguo la Funzionalita da testare:
		boolean result = controller.removeMeccanico(meccanico.getId());
		assertTrue(result);
		
		indirizzo_3 = controller.findIndirizzo(indirizzo_3.getId());
		assertNull(indirizzo_3); // L'Indirizzo dovrebbe essere stato cancellato a CASCATA (fatto automaticamente da Hibernate).
		
		List<Proprietario_Meccanico> lista_associazioni = controller.findAssociationsByMeccanicoId(meccanico.getId());
		assertTrue(lista_associazioni.isEmpty()); // Tutte le associazioni con quel Meccanico rimosso dovrebbe essere state cancellate dalla tabella di Join/Appoggio.
		
		LOGGER.info("Meccanico " + meccanico.getId() + " Rimosso dal DB!");
	}
	
	@Test
	public void testUpdateMeccanico() {
		
		Meccanico meccanico = controller.addMeccanico("Nome_Meccanico", "Cognome_Meccanico", "Specializzazione_Meccanico");
		Indirizzo indirizzo = controller.addIndirizzo("Mia_Nazione", "Mia_Citta", "Mia_Via", 1, meccanico);
		meccanico.setIndirizzo(indirizzo);
		
		// Eseguo la Funzionalita da testare:
		boolean result = controller.updateMeccanico(meccanico.getId(), "Nome_Meccanico_Aggiornato", "Cognome_Meccanico_Aggiornato", "Specializzazione_Meccanico_Aggiornata");
		assertTrue(result);
		
		LOGGER.info("Meccanico " + meccanico.getId() + " Aggiornato nel DB!");
	}
	
	@Test
	public void testFindMeccanico() {
		
		Meccanico meccanico = controller.addMeccanico("Nome_Meccanico", "Cognome_Meccanico", "Specializzazione_Meccanico");
		controller.addIndirizzo("Mia_Nazione", "Mia_Citta", "Mia_Via", 1, meccanico);
		int id_meccanico = meccanico.getId();
		
		// Eseguo la Funzionalita da testare:
		meccanico = controller.findMeccanico(id_meccanico);
		assertNotNull(meccanico);
		
		LOGGER.info("Meccanico " + id_meccanico + " Trovato nel DB!");
		System.out.println(meccanico.toString());
	}
	
	@Test
	public void testFindAllMeccanici() {
		
		Proprietario proprietario_1 = controller.addProprietario("Nome_Proprietario_1", "Cognome_Proprietario_1");
		controller.addIndirizzo("Nazione_1", "Citta_1", "Via_1", 1, proprietario_1);
		Proprietario proprietario_2 = controller.addProprietario("Nome_Proprietario_2", "Cognome_Proprietario_2");
		controller.addIndirizzo("Nazione_2", "Citta_2", "Via_2", 2, proprietario_2);
		
		Meccanico meccanico_1 = controller.addMeccanico("Nome_Meccanico_1", "Cognome_Meccanico_1", "Specializzazione_Meccanico_1");
		controller.addIndirizzo("Nazione_3", "Citta_3", "Via_3", 3, meccanico_1);
		Meccanico meccanico_2 = controller.addMeccanico("Nome_Meccanico_2", "Cognome_Meccanico_2", "Specializzazione_Meccanico_2");
		controller.addIndirizzo("Nazione_4", "Citta_4", "Via_4", 4, meccanico_2);
		Meccanico meccanico_3 = controller.addMeccanico("Nome_Meccanico_3", "Cognome_Meccanico_3", "Specializzazione_Meccanico_3");
		controller.addIndirizzo("Nazione_5", "Citta_5", "Via_5", 5, meccanico_3);
		
		// Inserisco l'associazione nella Tabella di Join/Appoggio tra il Meccanico ed il Proprietario:
		controller.addAssociationProprietarioMeccanico(new Proprietario_Meccanico(proprietario_1.getId(), meccanico_1.getId()));
		controller.addAssociationProprietarioMeccanico(new Proprietario_Meccanico(proprietario_1.getId(), meccanico_2.getId()));
		controller.addAssociationProprietarioMeccanico(new Proprietario_Meccanico(proprietario_2.getId(), meccanico_3.getId()));
		
		// Eseguo la Funzionalita da testare:
		List<Meccanico> lista_meccanici = controller.getAllMeccanici();
		assertNotNull(lista_meccanici);
		
		LOGGER.info("Meccanici Trovati nel DB!");
		for(Meccanico meccanico: lista_meccanici) {
			System.out.println(meccanico.toString());
		}
	}

}
