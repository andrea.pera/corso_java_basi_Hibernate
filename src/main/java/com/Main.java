package com;

import java.util.List;
import com.controller.AppController;
import com.model.entity.Automobile;
import com.model.entity.Indirizzo;
import com.model.entity.Meccanico;
import com.model.entity.Proprietario;

public class Main {

	public static void main(String[] args) {
		
		AppController controller = AppController.getInstance();
		
		List<Proprietario> lista_proprietari = controller.getAllProprietari();
		System.out.println("\n- - - - - Lista Proprietari: - - - - - :");
		for(Proprietario proprietario: lista_proprietari) {
			System.out.println(proprietario.toString() + "\n");
		}
		
		List<Automobile> lista_automobili = controller.getAllAutomobili();
		System.out.println("\n- - - - - Lista Automobili: - - - - - :");
		for(Automobile a: lista_automobili) {
			System.out.println(a.toString() + "\n");
		}
		
		List<Indirizzo> lista_indirizzi = controller.getAllIndirizzi();
		System.out.println("\n- - - - - Lista Indirizzi: - - - - - :");
		for(Indirizzo indirizzo: lista_indirizzi) {
			System.out.println(indirizzo.toString() + "\n");
		}
		
		List<Meccanico> lista_meccanici = controller.getAllMeccanici();
		System.out.println("\n- - - - - Lista Meccanici: - - - - - :");
		for(Meccanico meccanico: lista_meccanici) {
			System.out.println(meccanico.toString() + "\n");
		}
		
		System.exit(1);
	}

}
