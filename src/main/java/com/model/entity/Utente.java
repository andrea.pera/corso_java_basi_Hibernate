package com.model.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "Utente")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS) // Crea nel DataBase UNA TABELLA specifica PER OGNI SOTTO-CLASSE CONCRETA.
public abstract class Utente { // 'Utente' NON avrà una sua tabella nel DB.
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE) /* STESSA SEQUENZA DI IDENTIFICATORI per tutte le SOTTO-CLASSI CONCRETE.
												        -> Tabella 'hibernate_sequences' per tenere traccia degli ID già utilizzati. */
	protected int id;
	
	@Column(name = "nome")
	protected String nome;
	
	@Column(name = "cognome")
	protected String cognome;
	
	@OnDelete(action = OnDeleteAction.CASCADE) // Cancella automaticamente anche l'Indirizzo associato all'Utente cancellato.
	@OneToOne(mappedBy = "utente", // -> Nell'Indirizzo va messo: Attributo "utente", con getUtente() e setUtente().
		      cascade = CascadeType.ALL)
	protected Indirizzo indirizzo;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Indirizzo getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(Indirizzo indirizzo) {
		this.indirizzo = indirizzo;
	}
	
}
