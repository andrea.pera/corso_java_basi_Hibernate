package com.model.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Meccanico")
public class Meccanico extends Utente implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	// Eredita da Utente: Id, Nome, Cognome, Indirizzo.
	
	@Column(name = "specializzazione")
	private String specializzazione;
	
	@ManyToMany(mappedBy = "meccanici", // -> Nel Proprietario va messo: Attributo "meccanici", con getMeccanici() e setMeccanici().
			    fetch = FetchType.LAZY) /* "Caricamente Pigro": Quando prende un Meccanico dal DB, carica solo le informazioni presenti nella tabella del Meccanico. Quindi di default non prende i Proprietari associati.
										   Se si vogliono caricare anche i Proprietari, deve essere espressamente richiesto nel DAO con: Hibernate.initialize(meccanico.getProprietari()).
										   In questo modo, oltre alle informazioni di base, Hibernate caricherà anche i tutti i Proprietari associati ai Meccanici. */ 
    private Set<Proprietario> proprietari; // Clienti del Meccanico.

	public Meccanico() {}
	
	public Meccanico(String nome, String cognome, String specializzazione) {
		this.setNome(nome);
		this.setCognome(cognome);
		this.setSpecializzazione(specializzazione);
	}

	public Meccanico(int id, String nome, String cognome, String specializzazione) {
		this.setId(id);
		this.setNome(nome);
		this.setCognome(cognome);
		this.setSpecializzazione(specializzazione);
	}

	public String getSpecializzazione() {
		return specializzazione;
	}

	public void setSpecializzazione(String specializzazione) {
		this.specializzazione = specializzazione;
	}
	
	public Set<Proprietario> getProprietari() {
		return proprietari;
	}

	public void setProprietari(Set<Proprietario> proprietari) {
		this.proprietari = proprietari;
	}

	@Override
	public String toString() {
		return "\nID: " + this.id + "\n" +
			   "Nome: " + this.nome + "\n" +
			   "Cognome: " + this.cognome + "\n" +
			   "Specializzazione: " + this.specializzazione + "\n" +
			   "Indirizzo: { " + this.indirizzo + " }\n" +
			   "Lista Clienti: " + this.proprietari;
	}
	
	

}
