package com.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Automobile") // Hibernate creerà una tabella nel DB con quel nome.
public class Automobile implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id // L’attributo sarà la chiave primaria della tabella.
	@GeneratedValue(strategy = GenerationType.IDENTITY) // Fa sì che il campo sia incrementato e valorizzato autonomamente dal DB.
	private int id;
	
	@Column(name = "modello") // L'attributo sarà una colonna della tabella.
	private String modello;
	
	@Column(name = "marca")
	private String marca;
	
	@Column(name = "cilindrata")
	private int cilindrata;
	
	@Column(name = "sportiva")
	private boolean sportiva;
	
	@ManyToOne(optional = true) // Un'Automobile può NON avere un Proprietario.
    @JoinColumn(name = "id_proprietario", nullable = true)
	private Proprietario proprietario; // Nel DB è memorizzato solo con l'ID del proprietario.
	
	public Automobile() {}
	
	public Automobile(String modello, String marca, int cilindrata, boolean sportiva) {
		this.setModello(modello);
		this.setMarca(marca);
		this.setCilindrata(cilindrata);
		this.setSportiva(sportiva);
	}
	
	public Automobile(String modello, String marca, int cilindrata, boolean sportiva, Proprietario proprietario) {
		this.setModello(modello);
		this.setMarca(marca);
		this.setCilindrata(cilindrata);
		this.setSportiva(sportiva);
		this.setProprietario(proprietario);
	}
	
	public Automobile(int id, String modello, String marca, int cilindrata, boolean sportiva) {
		this.setId(id);
		this.setModello(modello);
		this.setMarca(marca);
		this.setCilindrata(cilindrata);
		this.setSportiva(sportiva);
	}
	
	public Automobile(int id, String modello, String marca, int cilindrata, boolean sportiva, Proprietario proprietario) {
		this.setId(id);
		this.setModello(modello);
		this.setMarca(marca);
		this.setCilindrata(cilindrata);
		this.setSportiva(sportiva);
		this.setProprietario(proprietario);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return modello;
	}
	
	public String getModello() {
		return modello;
	}

	public void setModello(String modello) {
		this.modello = modello;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getCilindrata() {
		return cilindrata;
	}

	public void setCilindrata(int cilindrata) {
		this.cilindrata = cilindrata;
	}

	public boolean getSportiva() {
		return sportiva;
	}

	public void setSportiva(boolean sportiva) {
		this.sportiva = sportiva;
	}

	public Proprietario getProprietario() {
		return proprietario;
	}

	public void setProprietario(Proprietario proprietario) {
		this.proprietario = proprietario;
	}

	@Override
	public String toString() {
		return "\nID: " + this.id + "\n" +
			   "Modello: " + this.modello + "\n" +
			   "Marca: " + this.marca + "\n" +
			   "Cilindrata: " + this.cilindrata + "\n" +
			   "Sportiva: " + this.sportiva + "\n" +
			   "ID Proprietario: " + (this.proprietario != null ? this.proprietario.getId() : "Null");
	}
	
	

}
