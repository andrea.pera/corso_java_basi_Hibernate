package com.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Indirizzo")
public class Indirizzo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "nazione")
	private String nazione;
	
	@Column(name = "citta")
	private String citta;
	
	@Column(name = "strada")
	private String strada;
	
	@Column(name = "numero_civico")
	private int numero_civico;
	
	//@OnDelete(action = OnDeleteAction.CASCADE) ?????????
	@OneToOne
	@JoinColumn(name = "id_utente", nullable = false, unique = true)
	private Utente utente; // Nel DB è memorizzato solo con l'ID dell'Utente.
	
	public Indirizzo() {}

	public Indirizzo(String nazione, String citta, String strada, int numero_civico) {
		this.setNazione(nazione);
		this.setCitta(citta);
		this.setStrada(strada);
		this.setNumero_civico(numero_civico);
	}
	
	public Indirizzo(String nazione, String citta, String strada, int numero_civico, Utente utente) {
		this.setNazione(nazione);
		this.setCitta(citta);
		this.setStrada(strada);
		this.setNumero_civico(numero_civico);
		this.setUtente(utente);
	}

	public Indirizzo(int id, String nazione, String citta, String strada, int numero_civico) {
		this.setId(id);
		this.setNazione(nazione);
		this.setCitta(citta);
		this.setStrada(strada);
		this.setNumero_civico(numero_civico);
	}
	
	public Indirizzo(int id, String nazione, String citta, String strada, int numero_civico, Utente utente) {
		this.setId(id);
		this.setNazione(nazione);
		this.setCitta(citta);
		this.setStrada(strada);
		this.setNumero_civico(numero_civico);
		this.setUtente(utente);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNazione() {
		return nazione;
	}

	public void setNazione(String nazione) {
		this.nazione = nazione;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getStrada() {
		return strada;
	}

	public void setStrada(String strada) {
		this.strada = strada;
	}

	public int getNumero_civico() {
		return numero_civico;
	}

	public void setNumero_civico(int numero_civico) {
		this.numero_civico = numero_civico;
	}

	public Utente getUtente() {
		return utente;
	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}

	@Override
	public String toString() {
		return "ID: " + this.id + ", " +
			   "Nazione: " + this.nazione + ", " +
			   "Citta: " + this.citta + ", " +	
			   "Strada: " + this.strada + ", " +	
			   "Numero Civico: " + this.numero_civico + ", " +
			   "ID_Utente: " + this.utente.getId();
	}

}
