package com.model.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "Proprietario")
public class Proprietario extends Utente implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	// Eredita da Utente: Id, Nome, Cognome, Indirizzo.
	
	@OneToMany(mappedBy = "proprietario", // -> Nell'Automobile va messo: Attributo "proprietario", con getProprietario() e setProprietario().
			   cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<Automobile> automobili; /* Non è memorizzato questo campo nella tabella del 'Proprietario',
										   perchè è memorizzato l'ID del proprietario nella tabella 'Automobile' e tramite quello ricavo la lista di automobili.*/
	
	@ManyToMany(cascade = CascadeType.ALL,
				fetch = FetchType.LAZY) // "Caricamente Pigro".
	@JoinTable(name = "Proprietario_Meccanico", // Tabella di JOIN che lega i Proprietari ai Meccanici, e viceversa (Tramite gli ID).
			   uniqueConstraints = { @UniqueConstraint(columnNames = {"id_proprietario", "id_meccanico"}) }, // = UNIQUE (id_proprietario, id_meccanico) <- Coppia Univoca.
			   joinColumns = { @JoinColumn(name = "id_proprietario") }, 
	           inverseJoinColumns = { @JoinColumn(name = "id_meccanico") })
	private Set<Meccanico> meccanici; // Meccanici da cui si serve un Proprietario di Auto.
	
	public Proprietario() {}
	
	public Proprietario(String nome, String cognome) {
		this.setNome(nome);
		this.setCognome(cognome);
	}
	
	public Proprietario(int id, String nome, String cognome) {
		this.setId(id);
		this.setNome(nome);
		this.setCognome(cognome);
	}
	
	public Set<Automobile> getAutomobili() {
		return automobili;
	}

	public void setAutomobili(Set<Automobile> automobili) {
		this.automobili = automobili;
	}

	public Set<Meccanico> getMeccanici() {
		return meccanici;
	}

	public void setMeccanici(Set<Meccanico> meccanici) {
		this.meccanici = meccanici;
	}

	@Override
	public String toString() {
		return "\nID: " + this.id + "\n" +
			   "Nome: " + this.nome + "\n" +
			   "Cognome: " + this.cognome + "\n" +
			   "Indirizzo: { " + this.indirizzo + " }\n";
			 //"Lista Automobili: " + this.automobili + "\n";
	}
	

}
