package com.model.util;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@IdClass(CompositeKey.class)
@Table(name = "Proprietario_Meccanico")
public class Proprietario_Meccanico {
	
	@Id
	@Column(name = "id_proprietario", nullable = false)
	private int id_proprietario;
	
	@Id
	@Column(name = "id_meccanico", nullable = false)
	private int id_meccanico;
	
	public Proprietario_Meccanico() {}
	
	public Proprietario_Meccanico(int id_proprietario, int id_meccanico) {
		this.setId_proprietario(id_proprietario);
		this.setId_meccanico(id_meccanico);
	}

	public int getId_proprietario() {
		return id_proprietario;
	}

	public void setId_proprietario(int id_proprietario) {
		this.id_proprietario = id_proprietario;
	}

	public int getId_meccanico() {
		return id_meccanico;
	}

	public void setId_meccanico(int id_meccanico) {
		this.id_meccanico = id_meccanico;
	}

	@Override
	public String toString() {
		return "ID Proprietario: " + this.id_proprietario + "\n" +
			   "ID Meccanico: " + this.id_meccanico + "\n";
	}

}
