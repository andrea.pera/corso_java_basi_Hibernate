package com.model.dao;

import java.sql.SQLException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateDataSource {
	
	private static final String HIBERNATE_CONFIG_FILENAME = "hibernate.cfg.xml";
	private static HibernateDataSource instance = null;
	private SessionFactory session_factory = null;

	private HibernateDataSource() throws SQLException {
	    this.session_factory = new Configuration().configure(HIBERNATE_CONFIG_FILENAME).buildSessionFactory();
	} 
	 
	public static HibernateDataSource getInstance() throws SQLException {
	    if (instance == null) {
	    	instance = new HibernateDataSource();
	    }
	    return instance;
	}
	
	public Session getConnection() throws SQLException {
		return this.session_factory.openSession();
	}
}
