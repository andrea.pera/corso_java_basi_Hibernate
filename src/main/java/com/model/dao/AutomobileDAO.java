package com.model.dao;

import java.util.List;
import javax.persistence.TypedQuery;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.model.entity.Automobile;

public class AutomobileDAO implements DAO<Automobile> {
	
	public AutomobileDAO() {}
	
	/**
	 * Inserisce una nuova Automobile nel DB:
	 */
	public Automobile insert(Automobile auto) {
			
        Session session = null;
        Transaction transaction = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	session.save(auto);
        	transaction.commit();
        }
        catch (Exception e) {
        	if(transaction != null) {
        		transaction.rollback();
            }
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return auto;	
	}
	
	/**
	 * Cancella un'Automobile dal DB.
	 */
	public boolean deleteById(int auto_id) {
		
		Automobile automobile = this.findById(auto_id);
		if(automobile == null) { return false;}
		
		boolean result = false;
        Session session = null;
        Transaction transaction = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	session.delete(automobile);
        	transaction.commit();
        	result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return result;
	}
	
	/**
	 * Aggiorna le informazioni di una Automobile nel DB:
	 */
	public boolean update(Automobile automobile) {
		
		if(this.findById(automobile.getId()) == null) { return false;} // Controllo se l'Automobile già esiste.
		
		boolean result = false;
        Session session = null;
        Transaction transaction = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	session.update(automobile);
        	transaction.commit();
        	result = true;
        }
        catch (Exception e) {
        	if(transaction != null) {
        		transaction.rollback();
            }
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return result;	
	}
	
	/**
	 * Prende dal DB l'Automobile identificata tramite l'ID:
	 */
	public Automobile findById(int id_auto) {
		
		Automobile automobile = null;
        Session session = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	automobile = (Automobile) session.get(Automobile.class, id_auto); // Restituisce l'Automobile o NULL se non esiste nel DB.
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return automobile;
	}
	
	/**
	 * Prende tutte le Automobile dal DB:
	 */
	public List<Automobile> findAll() {
		
		List<Automobile> lista_automobili = null;
        Session session = null;
        String query = "SELECT auto FROM Automobile auto"; // Prendo tutta la "riga" dell'Automobile.
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	
        	TypedQuery<Automobile> typed_query = session.createQuery(query, Automobile.class); // La classe dei valori di ritorno è Automobile !!!
        	
        	lista_automobili = typed_query.getResultList(); // Eseguo la Query.
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return lista_automobili;
	}
	
	/**
	 * Prende dal DB tutte le Automobile di un Proprietario specificato tramite ID:
	 */
	public List<Automobile> findByProprietarioId(int proprietario_id) {
		
		List<Automobile> lista_automobili = null;
        Session session = null;
        String query = "SELECT auto FROM Automobile auto WHERE id_proprietario = ?1"; // Prendo tutta la "riga" dell'Automobile.
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	
        	TypedQuery<Automobile> typed_query = session.createQuery(query, Automobile.class); // La classe dei valori di ritorno è Automobile !!!
        	typed_query.setParameter(1, proprietario_id);
        	
        	lista_automobili = typed_query.getResultList(); // Eseguo la Query.
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return lista_automobili;
	}

}
