package com.model.dao;

import java.util.List;
import javax.persistence.TypedQuery;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.model.entity.Meccanico;

public class MeccanicoDAO implements DAO<Meccanico> {
	
	public MeccanicoDAO() {}

	/**
	 * Inserisce un nuovo Meccanico nel DB:
	 */
	public Meccanico insert(Meccanico meccanico) {
		
        Session session = null;
        Transaction transaction = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	session.save(meccanico);
        	transaction.commit();
        }
        catch (Exception e) {
        	if(transaction != null) {
        		transaction.rollback();
            }
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return meccanico;
	}
	
	/**
	 * Cancella un Meccanico dal DB
	 * (Rimuove TUTTE le associazioni del Meccanico dalla tabella di Join/Appoggio).
	 */
	public boolean deleteById(int id_meccanico) {
		
		Meccanico meccanico = this.findById(id_meccanico);  // Controllo se il Meccanico esiste.
		if(meccanico == null) { return false;}
		
		boolean result = false;
        Session session = null;
        Transaction transaction = null;
        
        try {
        	// Rimuovo le associazioni tra il Meccanico ed i Proprietari nella tabella di Join/Appoggio:
        	Proprietario_MeccanicoDAO associazioneDAO = new Proprietario_MeccanicoDAO();
        	associazioneDAO.deleteByMeccanicoId(id_meccanico);
        	
        	// Posso cancellare il Meccanico (Non essendo più legato a nessuna Proprietario):
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	session.delete(meccanico);
        	transaction.commit();
        	result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return result;
	}
	
	/**
	 * Aggiorna le informazioni di un Meccanico nel DB:
	 */
	public boolean update(Meccanico meccanico) {
		
		if(this.findById(meccanico.getId()) == null) { return false;} // Controllo se il Meccanico già esiste.
		
		boolean result = false;
        Session session = null;
        Transaction transaction = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	session.update(meccanico);
        	transaction.commit();
        	result = true;
        }
        catch (Exception e) {
        	if(transaction != null) {
        		transaction.rollback();
            }
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return result;	
	}

	/**
	 * Prende dal DB il Meccanico identificato tramite l'ID:
	 */
	public Meccanico findById(int id_meccanico) {
		
		Meccanico meccanico = null;
        Session session = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	meccanico = (Meccanico) session.get(Meccanico.class, id_meccanico); // Restituisce il Meccanico o NULL se non esiste nel DB.
        	Hibernate.initialize(meccanico.getProprietari()); /* Richiedo esplicitamente ad Hibernate di caricare anche i Proprietari associati ai Meccanici.
        													     Altrimenti di Default, con il LAZY LOADING, lui non li carica. */
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return meccanico;
	}
	
	/**
	 * Prende tutti i Meccanici dal DB:
	 */
	public List<Meccanico> findAll() {
		
		List<Meccanico> lista_meccanici = null;
        Session session = null;
        String query = "SELECT meccanico FROM Meccanico meccanico"; // Prendo tutta la "riga" dell'Automobile.
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	
        	TypedQuery<Meccanico> typed_query = session.createQuery(query, Meccanico.class); // La classe dei valori di ritorno è Automobile !!!
        	
        	lista_meccanici = typed_query.getResultList(); // Eseguo la Query.

        	for(Meccanico meccanico: lista_meccanici) {
        		Hibernate.initialize(meccanico.getProprietari()); // Richiedo esplicitamente ad Hibernate di caricare anche i Proprietari associati ai Meccanici.
        													      // Altrimenti di Default, con il LAZY LOADING, lui non li carica.
        	}
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return lista_meccanici;
	}
	

}
