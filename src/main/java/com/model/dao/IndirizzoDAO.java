package com.model.dao;

import java.util.List;
import javax.persistence.TypedQuery;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.model.entity.Indirizzo;

public class IndirizzoDAO implements DAO<Indirizzo> {
	
	public IndirizzoDAO() {}

	/**
	 * Inserisce un nuovo Indirizzo nel DB:
	 */
	public Indirizzo insert(Indirizzo indirizzo) {
        
		Session session = null;
        Transaction transaction = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	session.save(indirizzo);
        	transaction.commit();
        }
        catch (Exception e) {
        	if(transaction != null) {
        		transaction.rollback();
            }
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return indirizzo;	
	}
	
	/**
	 * Cancella un Indirizzo dal DB.
	 */
	public boolean deleteById(int id_indirizzo) {
		
		Indirizzo indirizzo = this.findById(id_indirizzo);
		if(indirizzo == null) { return false;}
		
		boolean result = false;
        Session session = null;
        Transaction transaction = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	session.delete(indirizzo);
        	transaction.commit();
        	result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return result;
	}

	/**
	 * Aggiorna le informazioni di un Indirizzo nel DB:
	 */
	public boolean update(Indirizzo indirizzo) {
		
		if(this.findById(indirizzo.getId()) == null) { return false;} // Controllo se l'Indirizzo già esiste.
		 
		boolean result = false;
        Session session = null;
        Transaction transaction = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	session.update(indirizzo);
        	transaction.commit();
        	result = true;
        }
        catch (Exception e) {
        	if(transaction != null) {
        		transaction.rollback();
            }
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return result;	
	}
	
	/**
	 * Prende dal DB l'Indirizzo identificato tramite l'ID:
	 */
	public Indirizzo findById(int id_indirizzo) {
		
		Indirizzo indirizzo = null;
        Session session = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	indirizzo = (Indirizzo) session.get(Indirizzo.class, id_indirizzo); // Restituisce l'Indirizzo o NULL se non esiste nel DB.
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return indirizzo;
	}

	/**
	 * Prende tutti gli Indirizzo dal DB:
	 */
	public List<Indirizzo> findAll() {
		
		List<Indirizzo> lista_indirizzo = null;
        Session session = null;
        String query = "SELECT indirizzo FROM Indirizzo indirizzo"; // Prendo tutta la "riga" dell'Indirizzo.
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	
        	TypedQuery<Indirizzo> typed_query = session.createQuery(query, Indirizzo.class); // La classe dei valori di ritorno è Automobile !!!
        	
        	lista_indirizzo = typed_query.getResultList(); // Eseguo la Query.
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return lista_indirizzo;
	}
	
}
