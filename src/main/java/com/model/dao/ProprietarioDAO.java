package com.model.dao;

import java.util.List;
import javax.persistence.TypedQuery;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.model.entity.Automobile;
import com.model.entity.Proprietario;

public class ProprietarioDAO implements DAO<Proprietario>  {
	
	public ProprietarioDAO() {}
	
	/**
	 * Inserisce un nuovo Proprietario nel DB:
	 */
	public Proprietario insert(Proprietario proprietario) {
		
        Session session = null;
        Transaction transaction = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	session.saveOrUpdate(proprietario);
        	transaction.commit();
        }
        catch (Exception e) {
        	if(transaction != null) {
        		transaction.rollback();
            }
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return proprietario;	
	}
	
	/**
	 * Cancella un Proprietario dal DB
	 * (Mette a NULL il campo 'id_proprietario' delle SUE Automobili):
	 */
	public boolean deleteById(int id_proprietario) {
		
		Proprietario proprietario = this.findById(id_proprietario);  // Controllo se il Proprietario esiste.
		if(proprietario == null) { return false;}
		
		boolean result = false;
		List<Automobile> auto_proprietario = null;
        Session session = null;
        Transaction transaction = null;
        
        try {
        	// Prendo le Automobili del Proprietario e metto a NULL il campo 'id_proprietario':
        	AutomobileDAO automobileDAO = new AutomobileDAO();
        	auto_proprietario = automobileDAO.findByProprietarioId(id_proprietario);
        	for(Automobile auto: auto_proprietario) {
        		auto.setProprietario(null); // Hibernate imposterà: 'id_proprietario' = NULL
            	automobileDAO.update(auto);
        	}
        	
        	// Rimuovo le associazioni tra il Meccanico ed i Proprietari nella tabella di Join/Appoggio:
        	Proprietario_MeccanicoDAO associazioneDAO = new Proprietario_MeccanicoDAO();
        	associazioneDAO.deleteByProprietarioId(id_proprietario);
        	
        	// Posso cancellare il Proprietario (Non essendo più legato a nessuna Automobile):
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	session.delete(proprietario);
        	transaction.commit();
        	result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return result;
	}

	/**
	 * Aggiorna le informazioni di un Proprietario nel DB:
	 */
	public boolean update(Proprietario proprietario) {
		
		if(this.findById(proprietario.getId()) == null) { return false;} // Controllo se il Proprietario già esiste.
		
		boolean result = false;
        Session session = null;
        Transaction transaction = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	session.update(proprietario);
        	transaction.commit();
        	result = true;
        }
        catch (Exception e) {
        	if(transaction != null) {
        		transaction.rollback();
            }
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return result;	
	}

	/**
	 * Prende dal DB il Proprietario identificato tramite l'ID:
	 */
	public Proprietario findById(int id_proprietario) {
		
		Proprietario proprietario = null;
        Session session = null;
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	proprietario = (Proprietario) session.get(Proprietario.class, id_proprietario); // Restituisce il Proprietario o NULL se non esiste nel DB.
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return proprietario;
	}

	/**
	 * Prende tutti i Proprietari dal DB:
	 */
	public List<Proprietario> findAll() {
		
		List<Proprietario> lista_proprietari = null;
        Session session = null;
        String query = "SELECT prop FROM Proprietario prop"; // Prendo tutta la "riga" dell'Proprietario.
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	
        	TypedQuery<Proprietario> typed_query = session.createQuery(query, Proprietario.class); // La classe dei valori di ritorno è Proprietario !!!
        	
        	lista_proprietari = typed_query.getResultList(); // Eseguo la Query.
        	/*for(Proprietario proprietario: lista_proprietari) {
        		Hibernate.initialize(proprietario.getAutomobili()); // Richiedo esplicitamente ad Hibernate di caricare anche le Automobili associate ai Proprietari.
        													        // Altrimenti di Default, con il LAZY LOADING, lui non le carica.
        	}*/
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return lista_proprietari;
	}

}
