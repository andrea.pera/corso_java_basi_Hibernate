package com.model.dao;

import java.util.List;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import org.hibernate.Transaction;
import com.model.util.Proprietario_Meccanico;
import org.hibernate.HibernateException;
import org.hibernate.Session;

public class Proprietario_MeccanicoDAO {
	
	public Proprietario_MeccanicoDAO() {}
	
	public boolean insert(Proprietario_Meccanico associazione) {
		
        Session session = null;
        boolean result = false;
        Transaction transaction = null;
        Query query = null;
        String query_string = "INSERT INTO Proprietario_Meccanico (id_proprietario, id_meccanico) VALUES (?, ?)";
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	query = session.createSQLQuery(query_string);
        	query.setParameter(1, associazione.getId_proprietario());
        	query.setParameter(2, associazione.getId_meccanico());
        	query.executeUpdate();
        	transaction.commit();
        	result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
		return result;
	}
	
	/**
	 * Cancella TUTTE le Associazioni tramite l'ID del Meccanico.
	 */
	public boolean deleteByMeccanicoId(int id_meccanico) {
		
		boolean result = false;
        Session session = null;
        Transaction transaction = null;
        Query query = null;
        String query_string = "DELETE FROM Proprietario_Meccanico WHERE id_meccanico = ?1";
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	query = session.createQuery(query_string);
        	query.setParameter(1, id_meccanico);
        	query.executeUpdate();
        	transaction.commit();
        	result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return result;
	}
	
	/**
	 * Cancella TUTTE le Associazioni tramite l'ID del Proprietario.
	 */
	public boolean deleteByProprietarioId(int id_proprietario) {
		
		boolean result = false;
        Session session = null;
        Transaction transaction = null;
        Query query = null;
        String query_string = "DELETE FROM Proprietario_Meccanico WHERE id_proprietario = ?1";
        
        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	transaction = session.beginTransaction();
        	query = session.createQuery(query_string);
        	query.setParameter(1, id_proprietario);
        	query.executeUpdate();
        	transaction.commit();
        	result = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return result;
	}
	
	/**
	 * Prende TUTTE le Associazioni tramite l'ID del Meccanico.
	 */
	public List<Proprietario_Meccanico> findByMeccanicoId(int id_meccanico) {
		
		List<Proprietario_Meccanico> lista_associazioni = null;
        Session session = null;
        String query_string = "SELECT associazione FROM Proprietario_Meccanico associazione WHERE id_meccanico = ?1";

        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	TypedQuery<Proprietario_Meccanico> query = session.createQuery(query_string, Proprietario_Meccanico.class);
        	query.setParameter(1, id_meccanico);
        	
        	lista_associazioni = query.getResultList(); // Eseguo la Query.
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return lista_associazioni;
	}
	
	/**
	 * Prende TUTTE le Associazioni tramite l'ID del Proprietario.
	 */
	public List<Proprietario_Meccanico> findByProprietarioId(int id_proprietario) {
		
		List<Proprietario_Meccanico> lista_associazioni = null;
        Session session = null;
        String query_string = "SELECT associazione FROM Proprietario_Meccanico associazione WHERE id_proprietario = ?1";

        try {           
        	session = HibernateDataSource.getInstance().getConnection();
        	TypedQuery<Proprietario_Meccanico> query = session.createQuery(query_string, Proprietario_Meccanico.class);
        	query.setParameter(1, id_proprietario);
        	
        	lista_associazioni = query.getResultList(); // Eseguo la Query.
        }
        catch (Exception e) {
            e.printStackTrace();
        } 
        finally {
            if (session != null) {
                try {
                    session.close();
                } 
                catch (HibernateException e) {
                    e.printStackTrace();
                }
            }
        }
        
        return lista_associazioni;
	}

}
