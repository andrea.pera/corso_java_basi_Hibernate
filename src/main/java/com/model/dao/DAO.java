package com.model.dao;

import java.util.List;

public interface DAO<T> {
	
	T insert(T t);
	
	T findById(int id);
	
	boolean deleteById(int id);
	
	boolean update(T t);
	
	List<T> findAll();
}
