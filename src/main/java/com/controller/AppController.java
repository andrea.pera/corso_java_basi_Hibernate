package com.controller;

import java.util.List;
import com.model.dao.AutomobileDAO;
import com.model.dao.IndirizzoDAO;
import com.model.dao.MeccanicoDAO;
import com.model.dao.ProprietarioDAO;
import com.model.dao.Proprietario_MeccanicoDAO;
import com.model.entity.Automobile;
import com.model.entity.Indirizzo;
import com.model.entity.Meccanico;
import com.model.entity.Proprietario;
import com.model.entity.Utente;
import com.model.util.Proprietario_Meccanico;

public class AppController {
	
	 private static AppController instance = null;
	 private AutomobileDAO automobileDAO = null;
	 private ProprietarioDAO proprietarioDAO = null;
	 private IndirizzoDAO indirizzoDAO = null;
	 private MeccanicoDAO meccanicoDAO = null;
	 private Proprietario_MeccanicoDAO proprietario_MeccanicoDAO = null;

	 private AppController() {} 
	 
	 public static AppController getInstance() {
	     if (instance == null) {
	          instance = new AppController();
	          instance.automobileDAO = new AutomobileDAO();
	          instance.proprietarioDAO  = new ProprietarioDAO();
	          instance.indirizzoDAO = new IndirizzoDAO();
	          instance.meccanicoDAO = new MeccanicoDAO();
	          instance.proprietario_MeccanicoDAO = new Proprietario_MeccanicoDAO();
	     }
	     return instance;
	 }
	 
	 /* - - - - - - - - - - - - Automobile - - - - - - - - - - - : */
	 
	 public Automobile addAuto(String modello, String marca, int cilindrata, boolean sportiva, Proprietario proprietario) {
		 return automobileDAO.insert(new Automobile(modello, marca, cilindrata, sportiva, proprietario));
	 }
	 
	 public boolean removeAuto(int id_auto) {
		 return automobileDAO.deleteById(id_auto);
	 }
	 
	 public boolean updateAuto(int id, String modello, String marca, int cilindrata, boolean sportiva, Proprietario proprietario) {
		 return automobileDAO.update(new Automobile(id, modello, marca, cilindrata, sportiva, proprietario));
	 }
	 
	 public Automobile findAuto(int id_auto) {
		 return automobileDAO.findById(id_auto);
	 }
	 
	 public List<Automobile> findAutomobiliByProprietarioId(int id_proprietario) {
		 return automobileDAO.findByProprietarioId(id_proprietario);
	 }
	 
	 public List<Automobile> getAllAutomobili() {
		 return automobileDAO.findAll();
	 }
	 
	 /* - - - - - - - - - - - - Proprietario - - - - - - - - - - - : */
	 
	 public Proprietario addProprietario(String nome, String cognome) {
		 return proprietarioDAO.insert(new Proprietario(nome, cognome));
	 }
	 
	 public boolean removeProprietario(int id_proprietario) {
		 return proprietarioDAO.deleteById(id_proprietario);
	 }
	 
	 public boolean updateProprietario(int id, String nome, String cognome) {
		 return proprietarioDAO.update(new Proprietario(id, nome, cognome));
	 } 
	 
	 public Proprietario findProprietario(int id_proprietario) {
		 return proprietarioDAO.findById(id_proprietario);
	 }
	 
	 public List<Proprietario> getAllProprietari() {
		 return proprietarioDAO.findAll();
	 }
	 
	 /* - - - - - - - - - - - - Indirizzo - - - - - - - - - - - : */
	 
	 public Indirizzo addIndirizzo(String nazione, String citta, String strada, int numero_civico, Utente utente) {
		 return indirizzoDAO.insert(new Indirizzo(nazione, citta, strada, numero_civico, utente));
	 }
	 
	 public boolean removeIndirizzo(int id_indirizzo) {
		 return indirizzoDAO.deleteById(id_indirizzo);
	 }
	 
	 public boolean updateIndirizzo(int id, String nazione, String citta, String strada, int numero_civico, Utente utente) {
		 return indirizzoDAO.update(new Indirizzo(id, nazione, citta, strada, numero_civico, utente));
	 } 
	 
	 public Indirizzo findIndirizzo(int id_indirizzo) {
		 return indirizzoDAO.findById(id_indirizzo);
	 }
	 
	 public List<Indirizzo> getAllIndirizzi() {
		 return indirizzoDAO.findAll();
	 }
	 
	 /* - - - - - - - - - - - - Meccanico - - - - - - - - - - - : */
	 
	 public Meccanico addMeccanico(String nome, String cognome, String specializzazine) {
		 return meccanicoDAO.insert(new Meccanico(nome, cognome, specializzazine));
	 }
	 
	 public boolean removeMeccanico(int id_meccanico) {
		 return meccanicoDAO.deleteById(id_meccanico);
	 }
	 
	 public boolean updateMeccanico(int id, String nome, String cognome, String specializzazione) {
		 return meccanicoDAO.update(new Meccanico(id, nome, cognome, specializzazione));
	 } 
	 
	 public Meccanico findMeccanico(int id_meccanico) {
		 return meccanicoDAO.findById(id_meccanico);
	 }
	 
	 public List<Meccanico> getAllMeccanici() {
		 return meccanicoDAO.findAll();
	 }
	 
	 /* - - - - - - - - - - - - Associazioni - - - - - - - - - - - : */
	 
	 public boolean addAssociationProprietarioMeccanico(Proprietario_Meccanico associazione) {
		 return proprietario_MeccanicoDAO.insert(associazione);
	 }
	 
	 public List<Proprietario_Meccanico> findAssociationsByMeccanicoId(int id_meccanico) {
		 return proprietario_MeccanicoDAO.findByMeccanicoId(id_meccanico);
	 }
	 
	 public List<Proprietario_Meccanico> findAssociationsByProprietarioId(int id_proprietario) {
		 return proprietario_MeccanicoDAO.findByProprietarioId(id_proprietario);
	 }
	 
	 public boolean removeAssociationsByMeccanicoId(int id_meccanico) {
		 return proprietario_MeccanicoDAO.deleteByMeccanicoId(id_meccanico);
	 }
	 

}
